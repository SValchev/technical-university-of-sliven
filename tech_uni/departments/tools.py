import transliterate

from django.utils.text import slugify


def get_slug(inp):
    prefixes = [x[0] for x in inp.split() if len(x) > 1]
    result = ''.join(prefixes)
    trans_result = transliterate.translit(result, 'bg', reversed=True)

    return slugify(trans_result)
