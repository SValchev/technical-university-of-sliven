from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'(?P<slug>[\w-]+)/laboratories/$',
        view=views.DepartmentLaboratoryListView.as_view(),
        name='laboratories',
    ),
    url(
        regex=r'(?P<slug>[\w-]+)/$',
        view=views.DepartmentDetailView.as_view(),
        name='home',
    ),
]
