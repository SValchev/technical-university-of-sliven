import os

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from departments.tools import get_slug


def cover_image_upload_location(instance, filename):
    app_name = instance._meta.app_label
    model_name = instance._meta.model_name
    return os.path.join(app_name, model_name, instance.slug, filename)


class Department(models.Model):
    class Meta:
        verbose_name = _('Department')
        verbose_name_plural = _('Departments')

    slug = models.SlugField(unique=True, null=False, blank=True, verbose_name=_('slug'))
    name = models.CharField(max_length=150, verbose_name=_('name'))
    content = models.TextField(verbose_name=_('content'))
    struct_info = models.TextField(verbose_name=_('structure info'))

    cover_image = models.ImageField(upload_to=cover_image_upload_location, verbose_name=_('cover image'))

    def get_absolute_url(self):
        return reverse('departments:home', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name


@receiver(pre_save, sender=Department)
def setup_slug_field(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = get_slug(instance.name)


def laboratory_image_upload_location(instance, filename):
    app_name = instance._meta.app_label
    model_name = instance._meta.model_name
    return os.path.join(app_name, model_name, instance.department.slug, filename)


class Laboratory(models.Model):
    class Meta:
        verbose_name = _('Laboratory')
        verbose_name_plural = _('Laboratories')

    department = models.ForeignKey(Department, verbose_name=_('departament'))
    room_number = models.CharField(_('room number'), max_length=4, )
    name = models.CharField(_('name'), max_length=225, )
    content = models.TextField(_('content'))
    image = models.ImageField(upload_to=laboratory_image_upload_location)

    @property
    def fullname(self):
        return '{}: {}'.format(self.room_number, self.name)

    def __str__(self):
        return self.name
