# Create your views here.
from django.views import View
from django.views.generic import DetailView
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.detail import SingleObjectMixin

from departments.models import Department


class DepartmentDetailView(DetailView):
    model = Department
    context_object_name = 'department'
    template_name = 'departments/department_detail.html'


class DepartmentLaboratoryListView(DetailView):
    model = Department
    template_name = 'departments/laboratories_list.html'

    def get_context_data(self, **kwargs):
        context = super(DepartmentLaboratoryListView, self).get_context_data(**kwargs)
        context['laboratory_set'] = self.object.laboratory_set.all()
        return context
