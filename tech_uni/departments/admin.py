from django.contrib import admin

# Register your models here.
from departments.models import Department, Laboratory

admin.site.register(Department)
admin.site.register(Laboratory)
