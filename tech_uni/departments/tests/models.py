import os

from django import test
from django.conf import settings
from django.forms import model_to_dict

from departments import factory
from departments import tools
from departments.factory import create_laboratory
from departments.models import Department, Laboratory, \
    laboratory_image_upload_location


class TestDepartment(test.TestCase):
    def test_create_model_with_factory(self):
        data = factory.get_department_data()
        instance = Department.objects.create(**data)

        model_fields = model_to_dict(instance, exclude=('id', 'slug'))

        for field, value in model_fields.items():
            self.assertIn(field, data)
            self.assertEqual(value, data[field])

    def test_model_save_with_correct_slug(self):
        departament = factory.create_department()

        expect = tools.get_slug(departament.name)
        result = departament.slug

        self.assertEqual(expect, result)


class TestLaboratory(test.TestCase):
    maxDiff = None

    def test_create_model_with_factory(self):
        department = factory.create_department()
        data = factory.get_laboratory_data()
        instance = Laboratory.objects.create(department=department, **data)

        model_fields = model_to_dict(instance, exclude=('id', 'department'))

        for field, value in model_fields.items():
            self.assertIn(field, data)
            self.assertEqual(value, data[field])

        self.assertIsInstance(instance, Laboratory)

    def test_laboratory_image_upload_location(self):
        department = factory.create_department()

        instance = create_laboratory(department)
        filename = 'test_file.txt'

        current_path = laboratory_image_upload_location(instance, filename)
        expected_path = os.path.join('departments', 'laboratory',
            instance.department.slug, filename)

        self.assertEqual(expected_path, current_path)
