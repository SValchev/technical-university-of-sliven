from django.http import HttpResponse
from django.test import TestCase
from django.urls import reverse

from departments.factory import create_department, create_laboratory


class TestDepartmentDetailView(TestCase):
    def test_page_get(self):
        department = create_department()

        response = self.client.get(reverse('departments:home', kwargs={'slug': department.slug}))

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.context_data['department'], department)


class TestDepartmentLaboratoryListView(TestCase):
    def test_page_get(self):
        department = create_department()
        view_url = reverse('departments:laboratories', kwargs={'slug': department.slug})

        num_laboratories = 10
        for x in range(num_laboratories):
            create_laboratory(department)

        response = self.client.get(view_url)
        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.wsgi_request.path, view_url)

        for laboratory in department.laboratory_set.all():
            self.assertIn(laboratory, response.context_data['laboratory_set'])
