import transliterate
from django.utils.text import slugify
from faker import Faker

from django.test import TestCase

from ..tools import get_slug


class TestMixin(TestCase):
    def test_get_slug(self):
        fake = Faker('bg_BG')

        name = '{} {} {}'.format(fake.first_name_male(), fake.first_name_male(), fake.last_name_male())
        prefixes = ''.join([x[0] for x in name.split()])

        trans_result = transliterate.translit(prefixes, 'bg', reversed=True)
        expected = slugify(trans_result)

        result = get_slug(name)

        self.assertEqual(result, expected)
