import os

from django.conf import settings
from faker import Faker
import random

from departments.models import Department, Laboratory


def get_department_data():
    fake = Faker('bg_BG')

    return {
        'name': fake.company(),
        'content': fake.text(),
        'struct_info': fake.text(),
        'cover_image': os.path.join(settings.DEFAULT_STATICFILES_DIR, 'img', 'default_department_image.jpeg'),
    }


def get_laboratory_data():
    laboratory_names = ('Математика', 'Физика', 'Химия', 'Информатика', 'Механика')
    fake = Faker()
    return {
        'room_number': str(random.randint(1000, 9999)),
        'name': random.choice(laboratory_names),
        'content': fake.text(),
        'image': os.path.join(settings.DEFAULT_STATICFILES_DIR, 'img', 'default_laboratory.jpeg')
    }


def create_department(name=None):
    data = get_department_data()
    if name:
        data['name'] = name
    return Department.objects.create(**data)


def create_laboratory(department):
    data = get_laboratory_data()
    data.update({
        'department': department
    })
    return Laboratory.objects.create(**data)
