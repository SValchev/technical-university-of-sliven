from collections import OrderedDict

from django.http import QueryDict

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
    'django_extensions',
]

# Postgres db settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tech_uni',
        'USER': 'svalchev',
        'PASSWORD': 'secretpassword',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# This is where your static files live
# Here is where you should import files
