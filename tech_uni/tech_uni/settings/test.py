from .base import *

TEST = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    },
}

# Don't test migrations
MIGRATION_MODULES = {'iop_manager': None,
                     'account': None,
                     'iop_local_supplier': None,
                     'iop_special_coordinator': None,
                     'norse_email': None,
                     'iop_local_coordinator': None,
                     'norse_password_reset': None,
                     'iop_special_supplier': None, }

SITE_HOST = 'http://test'

USE_TZ = False

# Uses weak password hasher for speeding up the tests.
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]
