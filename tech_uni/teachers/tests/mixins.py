import os
import shutil

from accounts.factory import create_system_superuser, create_system_user
from departments.factory import create_department
from teachers.factory import create_teacher
from teachers.models import TeacherDepartment, Teacher


class SetUpLoginTeacherUserAndDepartmentMixin(object):
    USERNAME = 'test_user'
    PASSWORD = 'top_secret_password'

    user = None
    profile = None
    department = None

    def setUp(self):
        super(SetUpLoginTeacherUserAndDepartmentMixin, self).setUp()
        self.user = self.create_user_profile(username=self.USERNAME, password=self.PASSWORD)
        self.profile = self.user.get_profile()

        self.department = create_department('Математика, Физика и Химия')
        TeacherDepartment.objects.create(teacher=self.profile, department=self.department)

    def tearDown(self):
        super(SetUpLoginTeacherUserAndDepartmentMixin, self).tearDown()
        profile_image_path = os.path.dirname(self.profile.image.path)
        shutil.rmtree(profile_image_path)

    def create_user_profile(self, username=None, password=None, superuser=False, academic_title=None):
        create_user = create_system_superuser if superuser is False else create_system_user

        user = create_user(username=username, password=password)
        create_teacher(user, academic_title)

        return user

    def login_default_user(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
