from django.core.exceptions import ValidationError
from django.http import Http404
from django.test import TestCase
from django.urls import reverse

from accounts.factory import create_system_user
from departments.factory import create_department
from profiles import profile_settings
from .mixins import SetUpLoginTeacherUserAndDepartmentMixin
from ..factory import create_teacher, create_publication, create_school_subject
from ..models import Teacher, Publication, TeacherDepartmentLeader, TeacherDepartment


class TeacherCreateMixin(object):
    pass


class TestTeacher(SetUpLoginTeacherUserAndDepartmentMixin, TestCase):
    academic_choices = tuple(x[0] for x in Teacher.ACADEMIC_CHOICES)

    def test_create_teacher(self):
        teacher = self.profile

        self.assertEqual(self.user, teacher.user)
        self.assertEqual(type(teacher), Teacher)
        self.assertIn(teacher.academic_title, self.academic_choices)

    def test_academic_choices(self):
        all_choices = (
            Teacher.ASSISTANT,
            Teacher.CHIEF_ASSISTANT,
            Teacher.TEACHER,
            Teacher.SENIOR_TEACHER,
            Teacher.DOCENT,
            Teacher.PROFESSOR,
        )

        for ac in self.academic_choices:
            self.assertIn(ac, all_choices)

    def test_get_teacher_departament_return_none(self):
        self.profile.department = None
        self.profile.save()
        self.profile.refresh_from_db()

        departament = self.profile.get_department()

        self.assertIsNone(departament)

    def test_get_teacher_departament_return_departament(self):
        expect = self.department
        result = self.profile.get_department()

        self.assertEqual(expect, result)

    def test_get_absolute_url_raise_exception(self):
        self.profile.department = None
        self.profile.refresh_from_db()

        with self.assertRaises(Http404):
            self.profile.get_absolute_url()

    def test_is_habilitated(self):
        non_habilitated_user_list = self._create_non_habilitated_teachers()
        habilitated_user_list = self._create_habilitated_teachers()

        for profile in non_habilitated_user_list:
            self.assertFalse(profile.is_habilitated())

        for profile in habilitated_user_list:
            self.assertTrue(profile.is_habilitated())

    def test_manager_habilitated(self):
        non_habilitated_user_list = self._create_non_habilitated_teachers()
        habilitated_user_list = self._create_habilitated_teachers()

        for profile in Teacher.objects.habilitated():
            self.assertIn(profile, habilitated_user_list)
            self.assertNotIn(profile, non_habilitated_user_list)

    def test_manager_non_habilitated(self):
        non_habilitated_user_list = self._create_non_habilitated_teachers()
        habilitated_user_list = self._create_habilitated_teachers()

        for profile in Teacher.objects.non_habilitated():
            self.assertNotIn(profile, habilitated_user_list)
            self.assertIn(profile, non_habilitated_user_list)

    def test_get_dashboard_url(self):
        self.assertEqual(reverse('teachers:dashboard'), self.profile.get_dashboard_url())

    def test_dunder_str(self):
        expected = '{} {}'.format(self.profile.first_name, self.profile.last_name)
        self.assertEqual(str(self.profile), expected)

    def _create_habilitated_teachers(self):
        result = []
        for academic_title in Teacher.HABILITATED_ACADEMIC_TITLES:
            result.append(self.create_user_profile(academic_title=academic_title).get_profile())

        self.profile.academic_title = Teacher.DOCENT
        self.profile.save()
        result.append(self.profile)

        return result

    def _create_non_habilitated_teachers(self):
        result = []
        NON_HABILITATED_ACADEMIC_TITLES = [title for title, string in Teacher.ACADEMIC_CHOICES
                                           if title not in Teacher.HABILITATED_ACADEMIC_TITLES]
        for academic_title in NON_HABILITATED_ACADEMIC_TITLES:
            result.append(self.create_user_profile(academic_title=academic_title).get_profile())
        return result


class TestTeacherDepartmentLeader(SetUpLoginTeacherUserAndDepartmentMixin, TestCase):
    def test_clean_fail_teacher_whit_different_department(self):
        user2 = self.create_user_profile()
        profile_user2 = user2.get_profile()

        department = create_department()
        TeacherDepartment.objects.create(teacher=profile_user2, department=department)

        with self.assertRaises(ValidationError) as cm:
            TeacherDepartmentLeader.objects.create(teacher=profile_user2, department=self.department)
        self.assertIn('Teacher can not be from different department.', cm.exception.messages)

    def test_clean_fail_teacher_no_department(self):
        user2 = self.create_user_profile()
        with self.assertRaises(ValidationError) as cm:
            TeacherDepartmentLeader.objects.create(teacher=user2.get_profile(),
                                                   department=self.department)
        self.assertIn('Teacher can not be from different department.', cm.exception.messages)

    def test_clean_success(self):
        department_leader = TeacherDepartmentLeader.objects.create(teacher=self.profile, department=self.department)

        self.assertEqual(department_leader, self.profile.teacher_department_leader)
        self.assertEqual(department_leader, self.department.teacher_leader)


class TestPublication(TestCase):
    def setUp(self):
        self.user = create_system_user(
            profile_settings.TEACHER_PROFILE)
        self.teacher = create_teacher(self.user)
        return super(TestPublication, self).setUp()

    def test_create_publication_whit_factory(self):
        instance = create_publication(self.teacher)

        self.assertIsInstance(instance, Publication)

    def test_dunder_str(self):
        publication = create_publication(self.teacher)
        expected = '{}: {}...'.format(self.teacher.get_full_name(), publication.title[:25])

        self.assertEqual(str(publication), expected)


class TestSchoolSubject(SetUpLoginTeacherUserAndDepartmentMixin, TestCase):
    def test_create_school_subject(self):
        school_subject = create_school_subject(self.department, [self.profile, ])

        self.assertEqual(self.profile.school_subject_set.first().pk, school_subject.pk)
