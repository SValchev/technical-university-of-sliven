import datetime

from django import test
from django.conf import settings
from django.forms import model_to_dict
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.urls import reverse

from .mixins import SetUpLoginTeacherUserAndDepartmentMixin
from ..factory import create_publication, get_publication_data, \
    create_school_subject
from ..models import TeacherDepartment, TeacherDepartmentLeader, Teacher


class TestNonLoginMixin(object):
    view_url = None

    def test_non_logged_in_user_can_not_see_page(self):
        response = self.client.get(self.view_url)
        redirect_url = '{}?next={}'.format(reverse(settings.LOGIN_URL),
                                           self.view_url)

        self.assertRedirects(response, redirect_url)


class TestTeacherDashboard(TestNonLoginMixin,
                           SetUpLoginTeacherUserAndDepartmentMixin,
                           test.TestCase):
    view_url = reverse('teachers:dashboard')

    def test_teacher_dashboard_status(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.get(path=self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)

    def test_teacher_dashboard_context_data(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.get(path=self.view_url)

        self.assertEqual(response.context_data['department'], self.department)
        self.assertEqual(response.context_data['object'],
                         self.user.get_profile())
        self.assertEqual(response.context_data['view'].section, 'dashboard')


class TestTeacherDetailView(SetUpLoginTeacherUserAndDepartmentMixin,
                            test.TestCase):
    def test_teacher_detail_status(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.get(
            path=reverse('teachers:detail',
                         kwargs={'slug': self.department.slug,
                                 'teacher': self.profile.slug}))

        self.assertEqual(response.status_code, HttpResponse.status_code)

    def test_publication_list_same_as_users(self):
        """
        Test context data publications in the same
        """
        self.login_default_user()

        num_publications = 10
        for x in range(num_publications):
            create_publication(self.profile)

        response = self.client.get(
            path=reverse('teachers:detail',
                         kwargs={'slug': self.department.slug,
                                 'teacher': self.profile.slug}))

        for publication in self.profile.publication_set.all():
            self.assertIn(publication,
                          response.context_data['view'].get_context_data()[
                              'publication_list'])

    def test_another_user_publications_not_in_context(self):
        self.login_default_user()

        user_2 = self.create_user_profile()
        profile_2 = user_2.get_profile()

        num_publications = 10
        for x in range(num_publications):
            create_publication(profile_2)

        response = self.client.get(
            path=reverse('teachers:detail',
                         kwargs={'slug': self.department.slug,
                                 'teacher': self.profile.slug}))

        for publication in profile_2.publication_set.all():
            self.assertNotIn(publication,
                             response.context_data['publication_list'])


class TestTeacherPublicationListView(TestNonLoginMixin,
                                     SetUpLoginTeacherUserAndDepartmentMixin,
                                     test.TestCase):
    view_url = reverse('teachers:publication_list')

    def test_get_correct_page(self):
        self.login_default_user()
        response = self.client.get(self.view_url)

        self.assertEquals(response.status_code, HttpResponse.status_code)

    def test_context_data(self):
        self.login_default_user()

        num_publications_per_page = 5
        for x in range(num_publications_per_page):
            create_publication(self.profile)

        response = self.client.get(self.view_url)

        self.assertEquals(response.context_data['view'].section,
                          'publications')
        self.assertEquals(response.context_data['object'], self.profile)
        for publication in self.profile.publication_set.all():
            self.assertIn(publication,
                          response.context_data['publication_list'])


class TestTeacherPublicationCreateView(TestNonLoginMixin,
                                       SetUpLoginTeacherUserAndDepartmentMixin,
                                       test.TestCase):
    view_url = reverse('teachers:publication_create')
    maxDiff = None

    def test_page(self):
        self.login_default_user()
        response = self.client.get(self.view_url)

        self.assertEquals(response.status_code, HttpResponse.status_code)

    def test_context_data(self):
        self.login_default_user()
        response = self.client.get(self.view_url)

        self.assertEquals(response.context_data['object'], self.profile)

    def test_create_publication(self):
        self.login_default_user()
        data = get_publication_data()
        data.update({
            'teacher': self.profile.pk
        })

        self.client.post(self.view_url, data=data)

        expected_data = model_to_dict(self.profile.publication_set.first(),
                                      exclude=('id',))

        self.assertEquals(self.profile.publication_set.count(), 1)
        self.assertDictEqual(expected_data, data)


class TestTeacherPublicationDeleteView(SetUpLoginTeacherUserAndDepartmentMixin,
                                       test.TestCase):
    def test_non_logged_in_user_can_not_see_page(self):
        publication = create_publication(self.profile)
        view_url = reverse('teachers:publication_delete',
                           kwargs={'pk': publication.pk})

        response = self.client.get(view_url)
        redirect_url = '{}?next={}'.format(reverse(settings.LOGIN_URL),
                                           view_url)

        self.assertRedirects(response, redirect_url)

    def test_delete_object(self):
        self.login_default_user()
        publication = create_publication(self.profile)

        response = self.client.post(reverse('teachers:publication_delete',
                                            kwargs={'pk': publication.pk}))

        self.assertEquals(self.profile.publication_set.count(), 0)
        self.assertNotIn(publication, self.profile.publication_set.all())
        self.assertRedirects(response, reverse('teachers:publication_list'))

    def test_can_not_delete_not_owned_publication(self):
        self.login_default_user()

        user_2 = self.create_user_profile(username='username',
                                          password='secret_password')
        publication_user_2 = create_publication(user_2.get_profile())

        url_user_2_delete_publication = reverse('teachers:publication_delete',
                                                kwargs={
                                                    'pk': publication_user_2.pk})
        response = self.client.post(url_user_2_delete_publication)

        self.assertEquals(user_2.get_profile().publication_set.count(), 1)
        self.assertIn(publication_user_2,
                      user_2.get_profile().publication_set.all())
        self.assertRedirects(response, reverse('teachers:publication_list'))


class TestTeacherPublicationUpdateView(SetUpLoginTeacherUserAndDepartmentMixin,
                                       test.TestCase):
    publication = None
    maxDiff = None

    def setUp(self):
        super(TestTeacherPublicationUpdateView, self).setUp()
        self.publication = create_publication(self.profile)

    def test_non_logged_in_user_can_not_see_page(self):
        view_url = reverse('teachers:publication_edit',
                           kwargs={'pk': self.publication.pk})

        response = self.client.get(view_url)
        redirect_url = '{}?next={}'.format(reverse(settings.LOGIN_URL),
                                           view_url)

        self.assertRedirects(response, redirect_url)

    def test_user_update_context_successfully(self):
        self.login_default_user()

        print(type(self.profile))

        data = {
            'teacher': self.profile.pk,
            'title': 'Test Title',
            'journal': 'Test journal',
            'authors': 'Some test authors',
            'release_date': datetime.date.today()
        }
        url = reverse('teachers:publication_edit',
                      kwargs={'pk': self.publication.pk})

        response = self.client.post(url, data=data)

        self.publication.refresh_from_db()

        current_publication_data = model_to_dict(self.publication,
                                                 exclude=('id',))

        self.assertDictEqual(current_publication_data, data)
        self.assertRedirects(response, reverse('teachers:publication_list'))

    def test_user_can_not_update_others_publications(self):
        self.login_default_user()

        user2 = self.create_user_profile(username='user2',
                                         password='password2')
        publication = create_publication(user2.get_profile())

        test_publication_data = {
            'teacher': self.profile.pk,
            'title': 'Test Title',
            'journal': 'Test journal',
            'authors': 'Some test authors',
            'release_date': datetime.date.today()
        }

        response = self.client.post(
            reverse('teachers:publication_edit',
                    kwargs={'pk': publication.pk}),
            data=test_publication_data
        )

        self.assertEqual(response.status_code,
                         HttpResponseNotFound.status_code)

    def test_context_data(self):
        self.login_default_user()

        publication_update_url = reverse('teachers:publication_edit',
                                         kwargs={'pk': self.publication.pk})
        response = self.client.get(path=publication_update_url)

        self.assertEqual(response.context_data['object'], self.profile)

    def test_get_publication_that_do_not_own(self):
        self.login_default_user()

        user2 = self.create_user_profile()
        publication_user2 = create_publication(user2.get_profile())

        publication_update_url = reverse('teachers:publication_edit',
                                         kwargs={'pk': publication_user2.pk})

        response = self.client.get(path=publication_update_url)
        self.assertEqual(response.status_code,
                         HttpResponseNotFound.status_code)


class TestDepartmentInfoUpdateView(TestNonLoginMixin,
                                   SetUpLoginTeacherUserAndDepartmentMixin,
                                   test.TestCase):
    view_url = reverse('teachers:edit_department_info')

    def setUp(self):
        super(TestDepartmentInfoUpdateView, self).setUp()
        self.user.is_staff = True
        self.user.save()

    def test_non_staff_user_can_not_get_page(self):
        self.login_default_user()
        self.user.is_staff = False
        self.user.save()

        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code,
                         HttpResponseNotFound.status_code)

    def test_superuser_can_get_page(self):
        self.login_default_user()

        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.context_data['object'], self.profile)
        self.assertEqual(response.context_data['department'], self.department)
        self.assertEqual(response.context_data['view'].section,
                         'department_info')

    def test_update_page_successfully(self):
        self.login_default_user()

        data = {
            'content': 'New test content',
        }

        response = self.client.post(self.view_url, data)
        self.department.refresh_from_db()

        self.assertRedirects(response, self.view_url)
        self.assertEqual(self.department.content, data['content'])


class TestDepartmentStructUpdateView(TestNonLoginMixin,
                                     SetUpLoginTeacherUserAndDepartmentMixin,
                                     test.TestCase):
    view_url = reverse('teachers:edit_department_struct')

    def setUp(self):
        super(TestDepartmentStructUpdateView, self).setUp()
        self.user.is_staff = True
        self.user.save()

    def test_non_staff_user_can_not_get_page(self):
        self.login_default_user()
        self.user.is_staff = False
        self.user.save()

        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code,
                         HttpResponseNotFound.status_code)

    def test_superuser_can_get_page(self):
        self.login_default_user()

        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.context_data['object'], self.profile)
        self.assertEqual(response.context_data['department'], self.department)
        self.assertEqual(response.context_data['view'].section, 'structure')

    def test_update_page_successfully(self):
        self.login_default_user()

        data = {
            'struct_info': 'New test struct_info',
        }

        response = self.client.post(self.view_url, data)
        self.department.refresh_from_db()

        self.assertRedirects(response, self.view_url)
        self.assertEqual(self.department.struct_info, data['struct_info'])


class TestDepartmentTeacherStructureView(
    SetUpLoginTeacherUserAndDepartmentMixin, test.TestCase):
    view_url = None
    leader = None

    def setUp(self):
        super(TestDepartmentTeacherStructureView, self).setUp()
        self.view_url = reverse('teachers:department_structure',
                                kwargs={'slug': self.department.slug})
        self.leader = TeacherDepartmentLeader.objects.create(
            teacher=self.profile, department=self.department).teacher

    def test_get_page(self):
        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.context_data['object'], self.department)
        self.assertEqual(response.context_data['department'], self.department)

    def test_context_data(self):

        # Non-Habilitated
        self._create_teacher_department(academic_title=Teacher.TEACHER)
        # Habilitated
        self._create_teacher_department(academic_title=Teacher.DOCENT)

        response = self.client.get(self.view_url)

        self.assertEqual(response.context_data['leader'], self.leader)

        expected_habilitated_teachers = Teacher.objects.filter(
            academic_title__in=Teacher.HABILITATED_ACADEMIC_TITLES).exclude(
            id=self.leader.id)
        for teacher in expected_habilitated_teachers:
            self.assertIn(teacher, response.context_data['habilitated'])

        expected_habilitated_non_teachers = Teacher.objects.exclude(
            academic_title__in=Teacher.HABILITATED_ACADEMIC_TITLES).exclude(
            id=self.leader.id)
        for teacher in expected_habilitated_non_teachers:
            self.assertIn(teacher, response.context_data['non_habilitated'])

    def _create_teacher_department(self, academic_title):
        num_create_profiles = 10

        for x in range(num_create_profiles):
            user = self.create_user_profile(academic_title=academic_title)
            TeacherDepartment.objects.create(
                teacher=user.get_profile(), department=self.department)


class TestSchoolSubjectListView(TestNonLoginMixin,
                                SetUpLoginTeacherUserAndDepartmentMixin,
                                test.TestCase):
    view_url = reverse('teachers:school_subject_list')

    def setUp(self):
        super(TestSchoolSubjectListView, self).setUp()

        num_school_subjects = 10
        for x in range(num_school_subjects):
            create_school_subject(self.department, teachers=[self.profile, ])

    def test_get_school_subject_page(self):
        self.login_default_user()
        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)

    def test_context_data(self):
        self.login_default_user()

        response = self.client.get(self.view_url)
        response2 = self.client.get('{}?page=2'.format(self.view_url))

        for ss in self.profile.school_subject_set.all()[:5]:
            self.assertIn(ss, response.context_data['school_subject_list'])

        for ss in self.profile.school_subject_set.all()[5:]:
            self.assertIn(ss, response2.context_data['school_subject_list'])
