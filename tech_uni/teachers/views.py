from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.text import ugettext_lazy as _
from django.views import View
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic.detail import SingleObjectMixin

from accounts.mixin import StaffLoginRequiredMixin
from departments.models import Department, Laboratory
from teachers.forms import BasePublicationModelForm, PublicationCreateModelForm
from .forms import TeacherModelForm
from .models import Teacher, Publication, SchoolSubject


class TeacherBaseMixin(object):
    base = None

    def dispatch(self, request, *args, **kwargs):
        self.set_template_extend(request)
        return super(TeacherBaseMixin, self).dispatch(request, *args, **kwargs)

    def set_template_extend(self, request):
        if request.user.is_staff:
            self.base = 'teachers/staff_teacher.html'
        else:
            self.base = 'teachers/normal_teacher.html'


class TeacherObjectListMixin(object):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(TeacherObjectListMixin, self).get(request, *args,
                                                       **kwargs)

    def get_object(self):
        return self.request.user.get_profile()

    def get_context_data(self, **kwargs):
        context = super(TeacherObjectListMixin, self).get_context_data(
            **kwargs)
        context['object'] = self.object
        return context


class TeacherDashboard(LoginRequiredMixin, TeacherBaseMixin, TemplateView):
    template_name = 'teachers/dashboard.html'
    section = 'dashboard'

    def get_context_data(self, **kwargs):
        context = super(TeacherDashboard, self).get_context_data(**kwargs)
        context[
            'department'] = self.request.user.get_profile().get_department()
        context['object'] = self.get_object()
        return context

    def get_object(self):
        return self.request.user.get_profile()


class TeacherEditProfileView(LoginRequiredMixin, TeacherBaseMixin, UpdateView):
    model = Teacher
    form_class = TeacherModelForm

    template_name = 'teachers/edit_user_profile.html'
    success_url = reverse_lazy('teachers:edit_profile')

    section = 'profile_edit'

    def get_object(self, queryset=None):
        return self.request.user.get_profile()


class TeacherDetailView(DetailView):
    model = Teacher
    template_name = 'teachers/profile_detail.html'
    slug_url_kwarg = 'teacher'

    def get_context_data(self, **kwargs):
        context = super(TeacherDetailView, self).get_context_data(**kwargs)
        context['publication_list'] = self.object.publication_set.all()
        return context


class TeacherPublicationListView(LoginRequiredMixin, TeacherBaseMixin,
                                 TeacherObjectListMixin, ListView):
    model = Publication

    template_name = 'teachers/publication_list.html'
    paginate_by = 5

    context_object_name = 'publication_list'
    object = None

    section = 'publications'

    # TODO: Think that this post is unneeded
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return redirect(reverse('teachers:publications_list'))

    def get_queryset(self):
        return self.object.publication_set.all()


class TeacherPublicationCreateView(LoginRequiredMixin, TeacherBaseMixin,
                                   CreateView):
    model = Publication
    form_class = PublicationCreateModelForm

    template_name = 'teachers/publication_create.html'
    context_object_name = 'publication'

    success_url = reverse_lazy('teachers:publication_list')

    def get_form_kwargs(self):
        kwargs = super(TeacherPublicationCreateView, self).get_form_kwargs()
        data = kwargs.get('data')
        if data:
            kwargs['data'] = data.copy()
            kwargs['data'].update({
                'profile': self.request.user.get_profile()
            })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(TeacherPublicationCreateView, self).get_context_data(
            **kwargs)
        context['object'] = self.request.user.get_profile()
        return context


class TeacherPublicationUpdateView(LoginRequiredMixin, TeacherBaseMixin,
                                   UpdateView):
    model = Publication
    form_class = BasePublicationModelForm

    template_name = 'teachers/publication_edit.html'
    context_object_name = 'publication'

    success_url = reverse_lazy('teachers:publication_list')
    error_url = reverse_lazy('teachers:publication_list')

    error_message = _('Can not update publications that are not own by you.')

    def get_object(self, queryset=None):
        obj = super(TeacherPublicationUpdateView, self).get_object()
        if self.request.user.get_profile() != obj.teacher:
            raise Http404
        return obj

    def get_context_data(self, **kwargs):
        context = super(TeacherPublicationUpdateView, self).get_context_data(
            **kwargs)
        context['object'] = self.request.user.get_profile()
        return context


class TeacherPublicationDeleteView(LoginRequiredMixin, SingleObjectMixin,
                                   View):
    model = Publication

    success_url = reverse_lazy('teachers:publication_list')
    error_url = reverse_lazy('teachers:publication_list')

    success_message = 'Publication whit title {} was deleted successfully'
    error_message = 'Can not delete publications that you do not own.'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.delete_is_valid():
            return self.delete_valid()
        return self.delete_invalid()

    def delete_is_valid(self):
        return self.request.user.get_profile() == self.object.teacher

    def delete_valid(self):
        self.object.delete()
        messages.success(self.request,
                         self.success_message.format(self.object.title))
        return redirect(self.success_url)

    def delete_invalid(self):
        messages.error(self.request, self.error_message)
        return redirect(self.error_url)


class DepartmentInfoUpdateView(StaffLoginRequiredMixin, SuccessMessageMixin,
                               TeacherBaseMixin, UpdateView):
    model = Department

    fields = ('content',)

    context_object_name = 'department'
    template_name = 'teachers/department_info_edit.html'
    success_url = reverse_lazy('teachers:edit_department_info')

    success_message = _('Department info have been changed successfully.')

    section = 'department_info'

    def get_object(self, queryset=None):
        return self.request.user.get_profile().get_department()

    def get_context_data(self, **kwargs):
        context = super(DepartmentInfoUpdateView, self).get_context_data(
            **kwargs)
        context['object'] = self.request.user.get_profile()
        return context


class DepartmentStructUpdateView(StaffLoginRequiredMixin, SuccessMessageMixin,
                                 TeacherBaseMixin, UpdateView):
    model = Department

    fields = ('struct_info',)

    context_object_name = 'department'
    template_name = 'teachers/department_structure_edit.html'
    success_url = reverse_lazy('teachers:edit_department_struct')

    success_message = _('Department structure have been changed successfully.')

    section = 'structure'

    def get_object(self, queryset=None):
        return self.request.user.get_profile().get_department()

    def get_context_data(self, **kwargs):
        context = super(DepartmentStructUpdateView, self).get_context_data(
            **kwargs)
        context['object'] = self.request.user.get_profile()
        return context


class DepartmentTeacherStructureView(DetailView):
    model = Department
    template_name = 'teachers/department_structure.html'
    context_object_name = 'department'

    def get_context_data(self, **kwargs):
        context = super(DepartmentTeacherStructureView, self).get_context_data(
            **kwargs)

        participant_id_list = self.object.teacher_department.all().values_list(
            'teacher_id')
        participant_list = Teacher.objects.filter(id__in=participant_id_list)

        context['leader'] = self.object.teacher_leader.teacher
        context['habilitated'] = participant_list.habilitated()
        context['non_habilitated'] = participant_list.non_habilitated()

        return context


class SchoolSubjectListView(LoginRequiredMixin, TeacherBaseMixin,
                            TeacherObjectListMixin, ListView):
    model = SchoolSubject

    template_name = 'teachers/school_subject_list.html'
    paginate_by = 5

    context_object_name = 'school_subject_list'
    ordering = ('-created',)

    def get_queryset(self):
        return self.object.school_subject_set.all()


class SchoolSubjectEditView(TeacherBaseMixin, UpdateView):
    model = SchoolSubject
    fields = ('title', 'description')
    context_object_name = 'school_subject'

    template_name = 'teachers/school_subject_edit.html'

    def get_context_data(self, **kwargs):
        context = super(SchoolSubjectEditView, self).get_context_data(**kwargs)
        context['object'] = self.request.user.get_profile()
        return context


class LaboratoryUpdateListView(StaffLoginRequiredMixin, TeacherBaseMixin,
                               TeacherObjectListMixin, ListView):
    model = Laboratory
    template_name = 'teachers/laboratory_list.html'
    context_object_name = 'laboratory_list'
    paginate_by = 10

    def get_queryset(self):
        department = self.object.get_department()
        return department.laboratory_set.all()


class LaboratoryEditView(StaffLoginRequiredMixin, TeacherBaseMixin,
                         UpdateView):
    model = Laboratory

    fields = ('image', 'room_number', 'name', 'content', 'image',)

    template_name = 'teachers/laboratory_edit.html'
    context_object_name = 'laboratory'

    def get_context_data(self, **kwargs):
        context = super(LaboratoryEditView, self).get_context_data(**kwargs)
        context['object'] = self.request.user.get_profile()
        return context


class LaboratoryDeleteView(StaffLoginRequiredMixin, SingleObjectMixin, View):
    model = Laboratory

    success_url = reverse_lazy('teachers:laboratory_list')
    error_url = reverse_lazy('teachers:laboratory_list')

    success_message = 'Laboratory whit name {} was deleted successfully'
    error_message = 'Can not delete laboratory that you are not in.'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.delete_is_valid():
            return self.delete_valid()
        return self.delete_invalid()

    def delete_is_valid(self):
        teacher_profile = self.request.user.get_profile()
        is_from_same_department = teacher_profile.get_department() == self.object.department
        return is_from_same_department

    def delete_valid(self):
        self.object.delete()
        messages.success(self.request,
                         self.success_message.format(self.object.name))
        return redirect(self.success_url)

    def delete_invalid(self):
        messages.error(self.request, self.error_message)
        return redirect(self.error_url)


class LaboratoryCreateView(StaffLoginRequiredMixin, TeacherBaseMixin,
                           CreateView):
    model = Laboratory

    fields = ('room_number', 'name', 'content', 'image',)

    template_name = 'teachers/laboratory_create.html'
    context_object_name = 'laboratory'

    def get_context_data(self, **kwargs):
        context = super(LaboratoryCreateView, self).get_context_data(**kwargs)
        context['object'] = self.request.user.get_profile()
        return context
