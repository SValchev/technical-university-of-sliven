from django import forms

from teachers.models import Teacher, Publication


class TeacherModelForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = (
            'first_name', 'second_name', 'last_name',
            'email', 'phone_number', 'description',
        )


class BasePublicationModelForm(forms.ModelForm):
    class Meta:
        model = Publication
        fields = ('title', 'journal', 'authors', 'release_date')


class PublicationCreateModelForm(BasePublicationModelForm):
    def save(self, commit=True):
        instance = super(PublicationCreateModelForm, self).save(commit=False)
        instance.teacher = self.data.get('profile')
        if commit:
            instance.save()
        return instance
