from django.core.exceptions import ValidationError
from django.db import models
from django.http import Http404
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from departments.models import Department
from profiles.mixins import AbsUserProfile


class TeacherQuerySet(models.QuerySet):
    def habilitated(self):
        return self.filter(
            academic_title__in=self.model.HABILITATED_ACADEMIC_TITLES)

    def non_habilitated(self):
        return self.exclude(
            academic_title__in=self.model.HABILITATED_ACADEMIC_TITLES)


class Teacher(AbsUserProfile):
    class Meta:
        verbose_name = _('teacher profile')
        verbose_name_plural = _('teacher profiles')

    ASSISTANT = 1
    CHIEF_ASSISTANT = 2
    TEACHER = 3
    SENIOR_TEACHER = 4
    DOCENT = 5
    PROFESSOR = 6

    ACADEMIC_CHOICES = (
        (ASSISTANT, _('Assistant')),
        (CHIEF_ASSISTANT, _('Chief Assistant')),
        (TEACHER, _('Teacher')),
        (SENIOR_TEACHER, _('Senior Teacher')),
        (DOCENT, _('Docent')),
        (PROFESSOR, _('Professor'))
    )

    HABILITATED_ACADEMIC_TITLES = (DOCENT, PROFESSOR,)

    academic_title = models.PositiveSmallIntegerField(_('title'),
                                                      choices=ACADEMIC_CHOICES,
                                                      default=1)
    degree = models.CharField(_('Degree'), max_length=128)

    objects = TeacherQuerySet.as_manager()

    def is_habilitated(self):
        return self.academic_title in self.HABILITATED_ACADEMIC_TITLES

    def get_department(self):
        try:
            return self.department.department
        except models.ObjectDoesNotExist:
            return None

    def get_absolute_url(self):
        try:
            return reverse('teachers:detail',
                           kwargs={'slug': self.get_department().slug,
                                   'teacher': self.slug})
        except AttributeError:
            raise Http404

    def get_dashboard_url(self):
        return reverse('teachers:dashboard')

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class TeacherDepartment(models.Model):
    class Meta:
        verbose_name = _('Teacher Department')
        verbose_name_plural = _('Teachers Department')

    teacher = models.OneToOneField(Teacher, related_name='department')
    department = models.ForeignKey(Department,
                                   related_name='teacher_department')

    def __str__(self):
        return '{}: {}'.format(self.department, self.teacher)


class TeacherDepartmentLeader(models.Model):
    class Meta:
        verbose_name = _('Teacher Department Leader')
        verbose_name_plural = _('Teacher Departments Leaders')

    teacher = models.OneToOneField(Teacher,
                                   related_name='teacher_department_leader')
    department = models.OneToOneField(Department,
                                      related_name='teacher_leader')

    def save(self, **kwargs):
        self.full_clean()
        super(TeacherDepartmentLeader, self).save(**kwargs)

    def clean(self):
        if self.teacher.get_department() != self.department:
            raise ValidationError(
                _('Teacher can not be from different department.'))

    def __str__(self):
        return '{}: {}'.format(self.department, self.teacher)


class SchoolSubject(models.Model):
    class Meta:
        verbose_name = _('School Subject')
        verbose_name_plural = _('School Subjects')

    department = models.ForeignKey(Department,
                                   related_name='school_subject_set')
    teachers = models.ManyToManyField(Teacher,
                                      related_name='school_subject_set')
    title = models.CharField(_('title'), max_length=256)
    description = models.TextField(_('description'))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Publication(models.Model):
    class Meta:
        verbose_name = _('Publication')
        verbose_name_plural = _('Publications')

    teacher = models.ForeignKey(Teacher)
    title = models.CharField(_('Title'), max_length=512)
    journal = models.CharField(_('Journal'), max_length=512)
    authors = models.CharField(_('Author'), max_length=512)
    release_date = models.DateField(_('Release Date'))

    def __str__(self):
        return '{}: {}...'.format(self.teacher.get_full_name(),
                                  self.title[:25])
