import datetime
import random

from django.db import transaction
from faker import Faker

from profiles import factory
from .models import Teacher, Publication, SchoolSubject


def get_teacher_data(academic_title=None):
    data = factory.get_profile_data()

    academic_choices = list(dict(Teacher.ACADEMIC_CHOICES))
    academic_title = academic_title or random.choice(academic_choices)

    data.update({
        'academic_title': academic_title,
    })

    return data


def get_publication_data():
    fake = Faker('bg-BG')

    num_fake_names = 3
    fake_names = ', '.join([fake.name() for x in range(num_fake_names)])

    return {
        'title': fake.text(max_nb_chars=512),
        'authors': fake_names,
        'release_date': datetime.date.today(),
        'journal': fake.text(max_nb_chars=64),
    }


def get_school_subject_data():
    fake = Faker('bg-BG')
    return {
        'title': fake.text(max_nb_chars=256),
        'description': fake.text(),
    }


def create_teacher(user, academic_title=None):
    data = get_teacher_data(academic_title)
    return Teacher.objects.create(user=user, **data)


def create_publication(teacher):
    data = get_publication_data()
    return Publication.objects.create(teacher=teacher, **data)


def create_school_subject(department, teachers):
    data = get_school_subject_data()

    data['department'] = department
    school_subject = SchoolSubject.objects.create(**data)

    for t in teachers:
        school_subject.teachers.add(t)

    with transaction.atomic():
        school_subject.save()

    return school_subject
