from django.contrib.auth import login, update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import FieldDoesNotExist
from django.shortcuts import redirect

# Create your views here.
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView

from teachers.models import Teacher


class LoginView(TemplateView):
    template_name = 'accounts/login.html'

    def post(self, request, *args, **kwargs):
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(reverse('accounts:login_dispatch'))
        kwargs['form'] = AuthenticationForm()
        return super(LoginView, self).get(request, *args, **kwargs)

    def form_invalid(self, form):
        return self.render_to_response({'form': form})

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        next_url = self.request.GET.get('next', reverse('accounts:login_dispatch'))

        return redirect(next_url)


class PasswordChangeView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/password_change.html'

    def get(self, request, *args, **kwargs):
        form = PasswordChangeForm(request.user)
        context = self.get_context_data(form=form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = PasswordChangeForm(request.user, data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        return redirect(reverse('accounts:login_dispatch'))

    def form_invalid(self, form):
        context = self.get_context_data()
        context['form'] = form
        return self.render_to_response(context)


class LoginDispatcher(View):
    def get(self, request, *args, **kwargs):
        profile = request.user.get_profile()
        if isinstance(profile, Teacher):
            return redirect(reverse('teachers:dashboard'))
