from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import transaction
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from profiles import profile_settings


class SystemUserManager(BaseUserManager):
    def create_user(self, username, profile_type, password=None,
                    **kwargs):
        user = self._create_user(username, profile_type,
                                 password, **kwargs)
        with transaction.atomic():
            user.save()
        return user

    def create_superuser(self, username, profile_type,
                         password=None, **kwargs):

        user = self._create_user(username, profile_type,
                                 password, **kwargs)
        user.is_staff = True
        user.is_superuser = True
        with transaction.atomic():
            user.save()
        return user

    def _create_user(self, username, profile_type=None, password=None, **kwargs):
        user = self.model(
            username=username,
            profile_type=profile_type,
        )

        if not password:
            password = self.make_random_password()

        user.set_password(password)
        return user


class SystemUser(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    # Only username and password are required
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ('profile_type',)

    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=128,
        unique=True,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )

    profile_type = models.PositiveSmallIntegerField(
        choices=profile_settings.PROFILE_CHOICES,
        default=profile_settings.TEACHER_PROFILE,
    )

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    objects = SystemUserManager()

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def get_profile(self):
        return self.profile
