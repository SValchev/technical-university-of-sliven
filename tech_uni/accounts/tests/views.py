from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.test import TestCase
from django.urls import reverse
from django.conf import settings

from accounts.factory import create_system_user
from accounts.models import SystemUser
from teachers.factory import create_teacher
from teachers.models import Teacher


class RegisterTeacherUserMixin(object):
    USERNAME = 'username'
    PASSWORD = 'secret_password'

    user = None
    profile = None

    def setUp(self):
        super(RegisterTeacherUserMixin, self).setUp()

        self.user = create_system_user(username=self.USERNAME, password=self.PASSWORD)
        self.profile = create_teacher(self.user)

    def login_default_user(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)


class TestLoginView(RegisterTeacherUserMixin, TestCase):
    view_url = reverse('accounts:login')

    def test_teacher_login_successfully(self):
        data = {
            'username': self.USERNAME,
            'password': self.PASSWORD,
        }

        response = self.client.post(path=self.view_url, data=data, follow=True)

        self.assertRedirects(response, reverse('teachers:dashboard'))
        self.assertTrue(response.wsgi_request.user.is_authenticated())
        self.assertEqual(response.wsgi_request.user, self.user)
        self.assertIsInstance(response.wsgi_request.user, SystemUser)
        self.assertIsInstance(response.wsgi_request.user.get_profile(), Teacher)

    def test_teacher_wrong_password_and_username(self):
        data = {
            'username': 'invalid_username',
            'password': 'invalid_password',
        }

        response = self.client.post(path=self.view_url, data=data, follow=True)

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertFalse(response.wsgi_request.user.is_authenticated())
        self.assertTrue(response.wsgi_request.user.is_anonymous())
        self.assertIsInstance(response.wsgi_request.user, AnonymousUser)

    def test_get_login_page(self):
        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.wsgi_request.path, self.view_url)
        self.assertIsInstance(response.context_data['form'], AuthenticationForm)

    def test_redirect_if_user_is_logged_in(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponseRedirect.status_code)


class TestPasswordChangeView(RegisterTeacherUserMixin, TestCase):
    view_url = reverse('accounts:password_change')

    def test_get_change_password(self):
        self.login_default_user()
        response = self.client.get(self.view_url)

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertIsInstance(response.context_data['form'], PasswordChangeForm)

    def test_non_logged_in_user_redirects(self):
        response = self.client.get(self.view_url)
        redirect_url = '{}?next={}'.format(reverse('accounts:login'), self.view_url)

        self.assertRedirects(response, redirect_url)

    def test_successfully_change_password(self):
        self.login_default_user()
        NEW_PASSWORD = 'n3w_dummy_p@55w0rd'

        data = {
            'new_password1': NEW_PASSWORD,
            'new_password2': NEW_PASSWORD,
            'old_password': self.PASSWORD,
        }

        response = self.client.post(self.view_url, data=data, follow=True)
        self.user.refresh_from_db()

        self.assertRedirects(response, reverse('teachers:dashboard'))
        self.assertTrue(self.user.check_password(NEW_PASSWORD))

    def test_invalid_change_password(self):
        self.login_default_user()
        NEW_INVALID_PASSWORD1 = 'n3w_dummy_p@55w0rd_invalid1'
        NEW_INVALID_PASSWORD2 = 'n3w_dummy_p@55w0rd_invalid2'

        invalid_data = {
            'new_password1': NEW_INVALID_PASSWORD1,
            'new_password2': NEW_INVALID_PASSWORD2,
            'old_password': self.PASSWORD,
        }

        response = self.client.post(self.view_url, data=invalid_data, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response.wsgi_request.path, self.view_url)
        self.assertTrue(self.user.check_password(self.PASSWORD))


class TestLoginDispatcher(RegisterTeacherUserMixin, TestCase):
    login_url = reverse(settings.LOGIN_URL)

    def test_get_redirect_teacher_dashboard(self):
        data = {
            'username': self.USERNAME,
            'password': self.PASSWORD,
        }
        response = self.client.post(path=self.login_url, data=data, follow=True)

        self.assertRedirects(response, reverse('teachers:dashboard'))
