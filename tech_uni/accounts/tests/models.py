from django.test import TestCase

from profiles.profile_settings import TEACHER_PROFILE
from .. import factory
from ..models import SystemUser


class TestSystemUserManager(TestCase):
    USERNAME = 'username'
    PASSWORD = 'sec37_p@a55w0rd'

    def test_create_user(self):
        data = factory.get_system_user_data()
        user = SystemUser.objects.create_user(**data)

        # Testing does the create model caches the password
        self._test_password(user, data['password'])

        # Test permissions
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        data = factory.get_system_user_data()
        user = SystemUser.objects.create_superuser(**data)

        self._test_password(user, data['password'])

        # Test permissions
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)

    def _test_password(self, user, password):
        """
        Testing does the create model caches the password
        """
        self.assertNotEqual(password, user.password)
        self.assertTrue(user.check_password(password))

    def test_create_use_no_password_generate_random(self):
        user = SystemUser.objects.create_user(username=self.USERNAME, profile_type=TEACHER_PROFILE)

        self.assertIsNotNone(user)
        self.assertIsNotNone(user.password)

    def test_get_short_name(self):
        user = factory.create_system_user()
        self.assertEqual(user.get_short_name(), user.username)

    def test_get_full_name(self):
        user = factory.create_system_user()
        self.assertEqual(user.get_full_name(), user.username)
