from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^login/$',
        view=views.LoginView.as_view(),
        name='login',
    ),
    url(
        regex=r'^login-dispatch/$',
        view=views.LoginDispatcher.as_view(),
        name='login_dispatch',
    ),
    url(
        regex=r'^password-change/$',
        view=views.PasswordChangeView.as_view(),
        name='password_change',
    ),

]
