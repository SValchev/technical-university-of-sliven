from django.contrib import admin

# Register your models here.

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import SystemUser


class SystemUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'profile_type', 'is_staff')
    list_filter = ('is_staff', 'profile_type')
    fieldsets = (
        ('', {
            'fields': ('username', 'password')
        }),
        ('Profile info', {
            'fields': ('get_profile',)
        }),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser',
                       'groups', 'user_permissions')
        }),
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'profile_type', 'password1', 'password2')}
         ),
    )

    search_fields = ('username',)
    ordering = ('username',)
    filter_horizontal = ()
    readonly_fields = ('get_profile',)

    # def get_profile(self, obj):
    #     try:
    #         profile = obj.get_profile()
    #     except RuntimeError:
    #         return 'Profile missing'
    #     url_template = 'admin:{}_{}_change'.format(
    #         profile._meta.app_label, profile._meta.model_name
    #     )
    #     url = reverse(url_template, args=(profile.pk,))
    #
    #     return mark_safe('<a href="{}">{}</a>'.format(
    #         url, profile))

    # get_profile.short_description = 'Profile'

admin.site.register(SystemUser, SystemUserAdmin)
