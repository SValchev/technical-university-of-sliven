from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.http import HttpResponseRedirect


class StaffLoginRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        response = super(StaffLoginRequiredMixin, self).dispatch(request, *args, **kwargs)
        if response.status_code == HttpResponseRedirect.status_code:
            return response

        if not request.user.is_staff:
            raise Http404
        return response
