from faker import Factory

from profiles.profile_settings import DEFAULT_PROFILE
from .models import SystemUser


def get_system_user_data(username=None, password=None, profile_type=None, ):
    fake = Factory.create()
    return {
        'username': username or fake.user_name(),
        'password': password or fake.password(),
        'profile_type': profile_type or DEFAULT_PROFILE,
    }


def create_system_user(username=None, password=None, profile_type=None, ):
    data = get_system_user_data(username, password, profile_type)
    return SystemUser.objects.create_user(**data)


def create_system_superuser(username=None, password=None, profile_type=None):
    data = get_system_user_data(username, password, profile_type)
    return SystemUser.objects.create_superuser(**data)
