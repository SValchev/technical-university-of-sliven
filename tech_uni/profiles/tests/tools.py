import string
from faker import Faker

from django.test import TestCase

from ..tools import get_slug


class TestMixin(TestCase):
    def test_get_slug(self):
        fake = Faker('bg_BG')
        name = '{} {}'.format(fake.first_name_male(), fake.last_name_male())

        legal_chars = list(string.ascii_lowercase)
        legal_chars.append('-')

        slug_name = get_slug(name)

        for char in list(slug_name):
            self.assertIn(char, legal_chars)
