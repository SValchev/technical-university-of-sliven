"""
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='profile')
    slug = models.SlugField(verbose_name=_('slug'), unique=True, null=False, blank=True)

    first_name = models.CharField(verbose_name=_('first name'), max_length=100)
    second_name = models.CharField(verbose_name=_('second name'), max_length=100)
    last_name = models.CharField(verbose_name=_('last name'), max_length=100)

    email = models.EmailField(verbose_name=_('email'), blank=True, null=True)
    phone_number = models.CharField(verbose_name=_('phone number'), max_length=20, blank=True, null=True)
    description = models.TextField(verbose_name=_('description'))

"""
import os

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from faker import Faker


def get_profile_data():
    fake = Faker('bg_BG')
    result = {
        'first_name': fake.first_name_male(),
        'second_name': fake.first_name_male(),
        'last_name': fake.last_name_male(),

        'email': fake.email(),
        'phone_number': fake.phone_number(),
        'description': fake.text(),
    }

    with open(os.path.join(settings.DEFAULT_STATICFILES_DIR, 'img', 'default-avatar.jpeg'), 'rb') as file:
        result.update({
            'image': SimpleUploadedFile('default-avatar.jpeg', file.read())
        })

    return result
