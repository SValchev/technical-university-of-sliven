import transliterate

from django.utils.text import slugify


def get_slug(inp):
    trans_name = transliterate.translit(inp, 'bg', reversed=True)
    return slugify(trans_name)
