import os
import shutil

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .tools import get_slug


def profile_picture_upload_location(instance, filename):
    app_name = instance._meta.app_label
    model_name = instance._meta.model_name

    return os.path.join(app_name, model_name, instance.slug, filename)


class AbsUserProfile(models.Model):
    class Meta:
        abstract = True

    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='profile')
    slug = models.SlugField(verbose_name=_('slug'), unique=True, null=False,
                            blank=True)

    first_name = models.CharField(verbose_name=_('first name'), max_length=128)
    second_name = models.CharField(verbose_name=_('second name'),
                                   max_length=128)
    last_name = models.CharField(verbose_name=_('last name'), max_length=128)

    email = models.EmailField(verbose_name=_('email'), blank=True, null=True)
    phone_number = models.CharField(verbose_name=_('phone number'),
                                    max_length=64, blank=True, null=True)
    description = models.TextField(verbose_name=_('description'))

    # TODO: Not sure will I keep it i get confused from the SystemUser active
    # active = models.BooleanField(verbose_name=_('active'), default=True)
    image = models.ImageField(_('Image'), max_length=512,
                              upload_to=profile_picture_upload_location, )

    def save(self, *args, **kwargs):
        name = '{} {}'.format(self.first_name, self.last_name)
        self.slug = get_slug(name)
        super(AbsUserProfile, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        result = super(AbsUserProfile, self).delete(*args, **kwargs)

        folder = os.path.dirname(self.image.path)
        shutil.rmtree(folder)

        return result

    def get_full_name(self):
        return '{} {} {}'.format(self.first_name, self.second_name,
                                 self.last_name)

    def get_dashboard_url(self):
        raise NotImplementedError(
            'Method not implemented in {}'.format(self.__class__.__name__))
